import Vue from 'vue';
import App from './App.vue';

import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/ru-RU'
Vue.use(ElementUI, { locale });
import 'element-ui/lib/theme-chalk/index.css';

Vue.config.productionTip = false;

new Vue({
  el: '#app',
  render: h => h(App)
});
